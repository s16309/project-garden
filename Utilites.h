//
// Created by Przemek Urbaniak on 19.11.2017.
//

#ifndef GARDEN_UTILITES_H
#define GARDEN_UTILITES_H
#include <string>
#include <iostream>

class Utilites {

public:

    void get_plans();

    void get_shedules();

    void add_plan();

    void add_schedule();

    void add_section();

    bool check_plan_exists(int plan);

    void write_line(std::string msg);

    void separator();

};


#endif //GARDEN_UTILITES_H
