//
// Created by Przemek Urbaniak on 19.11.2017.
//

#include "Utilites.h"
#include <iostream>
#include <fstream>

using namespace std;

void Utilites::add_plan() {

    std::fstream plans;

    plans.open("/Users/kyez/ClionProjects/garden/plans.txt", std::ios::in);


    std::string name, section, last_line, str;
    int id;

    while(getline(plans, str)) {
        last_line = str;
    }


    id = stoi(last_line.substr(0, last_line.find(",")));
    id++;

    std::cout<<"Id nowego planu: "<<id<<std::endl;
    write_line("Podaj nazwe planu: ");

    std::cin.ignore();
    getline(std::cin, name);

    write_line("Nazwa planu: "+name);
    separator();
    write_line("Podaj numer sekcji: ");

    getline(std::cin, section);

    write_line("Numer sekcji: "+section);
    separator();

    plans.close();
    plans.open("/Users/kyez/ClionProjects/garden/plans.txt", std::ios::out | std::ios::app);



    plans<<std::endl<<id<<","<<name<<",s"<<section; //extract to function

    plans.close();

}

void Utilites::get_shedules() {
    fstream shedule;
    string str;

    shedule.open("/Users/kyez/ClionProjects/garden/shedule.txt", ios::in);

    Utilites::write_line("Obecny harmonogram: ");

    while(getline(shedule, str)) {
        Utilites::write_line(str);
    }
    shedule.close();
}

void Utilites::add_schedule() {
    int plan_number;

    Utilites::write_line("Podaj numer planu: ");
    cin>>plan_number;

    if(Utilites::check_plan_exists(plan_number)) {
        fstream shedule;
        string str, name, sequention, data;

        cout << "Plan o id: " << plan_number << " istnieje"<<endl;

        cin.ignore();
        Utilites::write_line("Podaj nazwe nowego wpisu:");
        getline(cin, name);
        Utilites::write_line("Podaj typ sekwencji(daily|weekday|hour");
        getline(cin, sequention);
        Utilites::write_line("Podaj wartosc");
        getline(cin, data);


        shedule.open("/Users/kyez/ClionProjects/garden/shedule.txt", ios::out | ios::app);

        shedule<<endl<<plan_number<<","<<name<<","<<sequention<<","<<data;
        Utilites::write_line("Nowy wpis w harmonogramie zostal utworzony!");

        shedule.close();

    } else {

        cout << "Plan o id: " << plan_number << " nie istnieje";

    }
}

void Utilites::add_section(){
    fstream sections;

    string name, section;
    int instruments[25];

    int id, inst_number;

    Utilites::write_line("Podaj nazwe identyfikatora dla nowej sekcji: ");
    cin>>id;
    Utilites::write_line("Podaj nazwe dla nowej sekcji");
    cin.ignore();
    getline(cin, name);

    Utilites::write_line("Podaj ile instrumentow zawiera sekcja (max: 25)");
    cin>>inst_number;

    for(int i = 0; i<inst_number; i++){
        int instument;
        cin>>instument;
        instruments[i] = instument;
    }

    sections.open("/Users/kyez/ClionProjects/garden/sections.txt", std::ios::out | std::ios::app);

    sections<<endl<<"s"<<id<<","<<name<<",["; //extract to function
    for(int i = 0; i<inst_number; i++) {
        sections << instruments[i];
        if(i!=inst_number-1){
            sections << ",";
        }
    }
    sections<<"]";

    sections.close();

    Utilites::write_line("Nowa sekcja zostala zapisana poprawnie");
}

bool Utilites::check_plan_exists(int plan_id) {
    fstream plan;
    string str;

    plan.open("/Users/kyez/ClionProjects/garden/plans.txt", ios::in);

    while(getline(plan, str)) {
        int id;
        id = stoi(str.substr(0, str.find(",")));

        if(id == plan_id) {
            return true;
        }

    }
    plan.close();
    return false;
}

void Utilites::write_line(std::string msg){
    std::cout<<msg<<std::endl;
}

void Utilites::separator(){
    Utilites::write_line("                                        ");
    Utilites::write_line("----------------------------------------");
    Utilites::write_line("                                        ");
}
