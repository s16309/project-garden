#include <iostream>
#include <fstream>
//#include <conio.h>
#include "Utilites.h"


using namespace std;


int main() {
    Utilites utility;

    int command;

    while(1) {
        command = 0;
        utility.write_line("Wybierz opcje: ");
        utility.write_line("1 - wyswietl harmonogram");
        utility.write_line("2 - utworz plan");
        utility.write_line("3 - utworz wpis harmonogramu");
        utility.write_line("4 - utworz nowa sekcje");
        utility.write_line("0 - zamkniecie managera");
        utility.write_line("00 - zamkniecie obu aplikacji");

        cin>>command;
        utility.separator();

        //make switch here
        if(command == 0){
            return 0;
        }

        if (command == 1) {
            utility.get_shedules();
        }

        if(command == 2) {
            utility.add_plan();
        }

        if(command == 3){
            utility.add_schedule();
        }

        if(command == 4){
            utility.add_section();
        }

        //system("clear");
    }

    return 0;
}